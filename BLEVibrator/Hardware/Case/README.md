# BLE Vibrator: 3D Printed Case

The two STL files, "CaseBottom.stl" and "CaseTop.stl" can be used to print a case for the BLE vibration motor.

I printed the parts on a Replicator 2 on "high" quality, no rafts, no supports. The 0.1mm layer height helps get a really good fit for the motor. The file "FullCase.thing" is designed to be used directly with MakerBot Desktop with the settings I used for a Replicator 2. The print time for one case is approcimately 3h8m and weighs 24g.

The case was designed in Autodesk Inventor. The two .ipt files are inlcuded in case any modifications are needed.